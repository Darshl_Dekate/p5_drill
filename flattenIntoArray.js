//3. Flatten the friends key into a basic array of names.


const givenArray = require('./index')

const newArray = givenArray.map(each =>{
    let array = each.friends.map(eachFriend => {
        return eachFriend.name
    })
    each.friends = array
    return each
})
console.log(newArray)