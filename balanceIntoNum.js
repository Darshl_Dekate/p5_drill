const givenArray = require('./index')
//1. Make the balance into a number. If the number is invalid, 
//default to 0. Do not overwrite the existing key, create a new one instead.
const output = givenArray.map((each) => {
    //console.log(val)
    let val = Number(each.balance.replace(/[$ ,]/g, ""))

    if (!Number.isNaN(val)) {
        each["modified_Balance"] = val;
    }
    else {
        each["modified_Balance"] = 0;
    }
    return each
})

console.log(output)
module.exports = output;