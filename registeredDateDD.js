const givenArray = require('./index')

function dateFormat(givenArray){
    let newArray = givenArray.map((element) => {
        let value = element["registered"].split("T");
        element['registered'] =  value[0].split("-").reverse().join("/");
        return element;
        
    })
    return newArray;
}
console.log(dateFormat(givenArray));