// //7. Calculate the total balance available.

const givenArray = require('./index')


const output5 = givenArray.reduce((acc, curr) => {

    let val = Number(curr.balance.replace(/[$ ,]/g, "").replace(",", ""))

    if (!Number.isNaN(val)) {
        acc = acc + val;

    }
    return acc

}, 0)

console.log(output5)