const givenArray = require('./index')
///2. Sort by descending order of age.

function compare(val1, val2) {

  if (val1["age"] > val2["age"]) {
    return -1;
  }
  if (val1["age"] < val2["age"]) {
    return 1;
  }
  return 0;

}
const output1 = givenArray.sort(compare)
console.log(output1)